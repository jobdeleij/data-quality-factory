from nameko.rpc import rpc
from dotenv import load_dotenv
import os
from uuid import UUID

from factories.database_factory import DatabaseFactory as dbf
from models.user_model import User


class UserWorker:

    current_dir = os.path.dirname(os.path.abspath(__file__))
    load_dotenv(dotenv_path="{}/.env".format(current_dir))

    # Mandatory field for service discovery
    name = os.getenv('WORKER_NAME')

    def __init__(self):
        self.db = dbf.get_database(os.getenv('DB_TYPE'))

    @rpc
    def get_users(self):
        users = self.db.users.get_users()
        return None if len(users) <= 0 else [o.__dict__ for o in users]

    @rpc
    def get_user_by_user_id(self, user_id):
        user_id = UUID(user_id)
        user = self.db.users.get_user_by_user_id(user_id)
        return user.__dict__ if user is not None else None

    @rpc
    def create_user(self, user):
        user_to_create = User(**user)
        res = self.db.users.create_user(user_to_create)
        return res.__dict__
