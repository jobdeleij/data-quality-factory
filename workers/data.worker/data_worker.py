from nameko.rpc import rpc
from dotenv import load_dotenv
import os
from factories.database_factory import DatabaseFactory as dbf
from models.dataset_model import Dataset


class DataWorker:

    current_dir = os.path.dirname(os.path.abspath(__file__))
    load_dotenv(dotenv_path="{}/.env".format(current_dir))

    # Mandatory field for service discovery
    name = os.getenv('WORKER_NAME')

    def __init__(self):
        self.db = dbf.get_database(os.getenv('DB_TYPE'))

    @rpc
    def get_datasets(self):
        datasets = self.db.datasets.get_datasets()
        return None if len(datasets) <= 0 else [d.__dict__ for d in datasets]

    @rpc
    def create_dataset(self, dataset):
        dataset_to_create = Dataset(**dataset)
        res = self.db.datasets.create_dataset(dataset_to_create)
        return res.__dict__
