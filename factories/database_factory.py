from services.local_database.local_database import LocalDatabase


class DatabaseFactory:

    def __init__(self):
        pass

    @staticmethod
    def get_database(db_type):
        if db_type == 'local':
            return LocalDatabase()
        return None
