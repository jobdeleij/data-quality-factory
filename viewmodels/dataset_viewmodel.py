from typing import Any, List
from pydantic import BaseModel
from uuid import UUID
from hateoas import hateoas
from hateoas.hateoas import Link
from datetime import datetime


class DatasetView(BaseModel):

    dataset_id: UUID
    name: str
    source: str
    uploaded: datetime
    bucket_name: str
    prefix: str
    filename: str
    links: List[Link] = []

    def __init__(self, **data: Any):
        super().__init__(**data)
        self.links.append(hateoas.generate_get_link('http://localhost:80/', 'data', self.dataset_id))

    def __str__(self):
        return str(self.dict())
