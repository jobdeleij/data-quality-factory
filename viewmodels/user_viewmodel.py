from typing import Any, List
from pydantic import BaseModel
from uuid import UUID
from hateoas import hateoas
from hateoas.hateoas import Link


class UserView(BaseModel):

    user_id: UUID
    name: str
    links: List[Link] = []

    def __init__(self, **data: Any):
        super().__init__(**data)
        self.links.append(hateoas.generate_get_link('http://localhost:80/', 'users', self.user_id))

    def __str__(self):
        return str(self.dict())
