from aws.aws_helper_classes import S3Helper
from fastapi import APIRouter, HTTPException, File, UploadFile
from nameko.standalone.rpc import ServiceRpcProxy
from dotenv import load_dotenv
import os

from api.requests.data_requests import CreateDatasetRequest
from api.responses.data_responses import GetDatasetsResponse, CreateDatasetResponse, UploadCsvResponse
from models.dataset_model import Dataset
from viewmodels.dataset_viewmodel import DatasetView

current_dir = os.path.dirname(os.path.abspath(__file__))
load_dotenv(dotenv_path="{}/.env".format(current_dir))

router = APIRouter()


def rpc_proxy():
    config = {'AMQP_URI': os.getenv('AMQP_URI')}
    service = os.getenv('DATA_WORKER')
    return ServiceRpcProxy(service, config)


@router.get('/', response_model=GetDatasetsResponse)
async def get_datasets():
    try:
        # Connect to service worker
        with rpc_proxy() as rpc:
            # Get all datasets
            rpc_datasets = rpc.get_datasets()
            datasets = [Dataset(**d) for d in rpc_datasets] if rpc_datasets is not None else None

            # Preparing response
            datasets_views = [DatasetView(**d.__dict__) for d in datasets] if datasets is not None else None
            return GetDatasetsResponse(datasets=datasets_views)

    except ConnectionError:
        raise HTTPException(status_code=111, detail="Broker not available")


@router.post('/', response_model=CreateDatasetResponse)
async def create_dataset(create_dataset_request: CreateDatasetRequest):
    try:
        # Connect to service worker
        with rpc_proxy() as rpc:
            # Preparing dataset to persist
            dataset = Dataset(**create_dataset_request.dict())
            dataset.bucket_name = os.getenv('BUCKET_NAME')
            if create_dataset_request.file_type == 'CSV':
                dataset.prefix = os.getenv('CSV_PREFIX')

            # Send create task to service worker
            rpc.create_dataset(dataset.__dict__)

            # Preparing response
            dataset_view = DatasetView(**dataset.__dict__)
            res = CreateDatasetResponse(dataset=dataset_view)
            return res

    except ConnectionError:
        raise HTTPException(status_code=111, detail="Broker not available")


@router.post('/upload/csv', response_model=UploadCsvResponse)
async def upload_csv(file: UploadFile = File(...)):
    try:
        bucket_name = os.getenv('BUCKET_NAME')
        prefix = os.getenv('CSV_PREFIX')
        bucket = S3Helper(profile_name='pon', bucket_name=bucket_name, prefix=prefix)
        filename = file.filename
        csv_bytes = await file.read()
        bucket.upload_file(filename, body=csv_bytes.decode(), json_file=False)
        return UploadCsvResponse(bucket_name=bucket_name, prefix=prefix, filename=filename)
    except ConnectionError:
        raise HTTPException(status_code=111, detail="Broker not available")
# except Exception:
#     raise HTTPException(status_code=111, detail="Could not connect to S3 bucket")
