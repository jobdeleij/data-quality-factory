import logging
from fastapi import APIRouter, HTTPException
from nameko.standalone.rpc import ServiceRpcProxy
from dotenv import load_dotenv
import os
from api.requests.user_requests import CreateUserRequest
from api.responses.user_responses import CreateUserResponse, GetUsersResponse, GetUserByIdResponse
from models.user_model import User
from viewmodels.user_viewmodel import UserView

current_dir = os.path.dirname(os.path.abspath(__file__))
load_dotenv(dotenv_path="{}/.env".format(current_dir))

router = APIRouter()


def rpc_proxy():
    config = {'AMQP_URI': os.getenv('AMQP_URI')}
    service = os.getenv('USER_WORKER')
    return ServiceRpcProxy(service, config)


@router.get('/', response_model=GetUsersResponse)
async def get_users():

    try:
        # Connect to service worker
        with rpc_proxy() as rpc:
            # Get all users
            users = [User(**u) for u in rpc.get_users()]
            [logging.warning(u.__dict__) for u in users]

            # Preparing response
            users_views = [UserView(**u.__dict__) for u in users]
            return GetUsersResponse(users=users_views)

    except ConnectionError:
        raise HTTPException(status_code=111, detail="Broker not available")


@router.post('/', response_model=CreateUserResponse)
async def create_user(create_user_request: CreateUserRequest):

    try:
        # Connect to service worker
        with rpc_proxy() as rpc:
            # Preparing user to persist
            user = User(**create_user_request.dict())

            # Send create task to service worker
            rpc.create_user(user.__dict__)

            # Preparing response
            user_view = UserView(**user.__dict__)
            res = CreateUserResponse(user=user_view)
            return res

    except ConnectionError:
        raise HTTPException(status_code=111, detail="Broker not available")


@router.get('/{user_id}', response_model=GetUserByIdResponse)
async def get_user_by_order_id(user_id: str):

    try:
        # Connect to service worker
        with rpc_proxy() as rpc:
            # Get user by id
            user = rpc.get_user_by_user_id(user_id)
            # User found
            if user is not None:
                # Preparing response
                u = User(**user)
                user_view = UserView(**u.__dict__)
                return GetUserByIdResponse(user=user_view)

            # User not found
            else:
                raise HTTPException(status_code=404, detail="Item not found")

    except ConnectionError:
        raise HTTPException(status_code=111, detail="Broker not available")
