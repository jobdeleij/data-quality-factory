from pydantic import BaseModel


class CreateDatasetRequest(BaseModel):
    name: str
    source: str
    filename: str
    file_type: str
