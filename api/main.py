from starlette.responses import Response

from starlette.exceptions import HTTPException as StarletteHTTPException
from fastapi import Depends, FastAPI, Header
from api.routers import users, datasets
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI(title="Data Quality Factory API",
              description="Auto-generated Swagger documentation",
              version="0.0.1",
              )

origins = [
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def get_token_header(x_token: str = Header(...)):
    if x_token != 'test':
        raise StarletteHTTPException(status_code=400, detail="X-Token header invalid")


@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    return Response(exc.detail, status_code=exc.status_code)


app.include_router(
    users.router,
    prefix="/users",
    tags=["users"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    datasets.router,
    prefix="/data",
    tags=["data"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)
