from typing import List, Any
from pydantic import BaseModel

from viewmodels.dataset_viewmodel import DatasetView


class GetDatasetsResponse(BaseModel):
    datasets: List[DatasetView] = []

    def __init__(self, datasets, **data: Any):
        super().__init__(**data)
        self.datasets = datasets


class CreateDatasetResponse(BaseModel):
    dataset: DatasetView = None

    def __init__(self, dataset, **data: Any):
        super().__init__(**data)
        self.dataset = dataset


class UploadCsvResponse(BaseModel):
    bucket_name: str = ''
    prefix: str = ''
    filename: str = ''

    def __init__(self, bucket_name, prefix, filename, **data: Any):
        super().__init__(**data)
        self.bucket_name = bucket_name
        self.prefix = prefix
        self.filename = filename
