from typing import Any, List

from pydantic import BaseModel

from viewmodels.user_viewmodel import UserView


class CreateUserResponse(BaseModel):
    user: UserView = None

    def __init__(self, user, **data: Any):
        super().__init__(**data)
        self.user = user


class GetUsersResponse(BaseModel):
    users: List[UserView] = []

    def __init__(self, users, **data: Any):
        super().__init__(**data)
        self.users = users


class GetUserByIdResponse(BaseModel):
    user: UserView = None

    def __init__(self, user, **data: Any):
        super().__init__(**data)
        self.user = user
