from typing import Any

from pydantic import BaseModel


class Link(BaseModel):
    href: str = ''
    rel: str = ''
    type: str = ''

    def __init__(self, href, rel, http_method, **data: Any):
        super().__init__(**data)
        self.href = href
        self.rel = rel
        self.type = http_method


def generate_get_link(base_url, relation, object_id):
    return Link(href='{}{}/{}'.format(base_url, relation, object_id), rel=relation, http_method='GET')
