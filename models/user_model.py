from uuid import uuid4, UUID


class User:

    user_id: UUID
    name: str

    def __init__(self, user_id=None, name=''):
        self.name = name
        self.user_id = user_id if user_id is not None else uuid4()

    def __str__(self):
        return str(self.__dict__)
