from uuid import uuid4, UUID
from datetime import datetime


class Dataset:

    dataset_id: UUID
    name: str
    source: str
    uploaded: datetime
    bucket_name: str = ''
    prefix: str = ''
    filename: str = ''
    file_type: str = ''

    def __init__(self, dataset_id: UUID = None, name='', source='', uploaded=None, bucket_name='', prefix='', filename='', file_type=''):
        self.uploaded = uploaded if uploaded is not None else datetime.utcnow()
        self.source = source
        self.name = name
        self.dataset_id = dataset_id if dataset_id is not None else uuid4()
        self.bucket_name = bucket_name
        self.prefix = prefix
        self.filename = filename
        self.file_type = file_type

    def __str__(self):
        return str(self.__dict__)
