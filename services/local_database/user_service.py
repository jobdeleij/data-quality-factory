from models.user_model import User


class UserService:

    users = None

    def __init__(self):
        user1 = User(name='John')
        user2 = User(name='Alice')
        self.users = [user1, user2]

    def get_users(self):
        return self.users

    def get_user_by_user_id(self, user_id):
        return next((x for x in self.users if x.user_id == user_id), None)

    def create_user(self, user):
        self.users.append(user)
        return user
