from .data_service import DataService
from .user_service import UserService


class LocalDatabase:
    users = UserService()
    datasets = DataService()
