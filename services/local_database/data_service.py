from models.dataset_model import Dataset


class DataService:

    def __init__(self):
        self.datasets = []

    def get_datasets(self):
        return self.datasets

    def create_dataset(self, dataset):
        self.datasets.append(dataset)
        return dataset
