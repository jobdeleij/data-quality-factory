import datetime
import json
import io

import boto3
from loguru import logger

# from .template_container_definition import generate_template


# TODO
# Think about json storage in database or s3


class Boto3Helper:
    """
    This is the Boto3Helper class which is the base class for creating subsequent classes based on AWS services.
    """

    def __init__(self, profile_name=None, aws_service=None, aws_session_type="client"):
        """ Init class for creating an Boto3Helper object

        Args:
            profile_name (str): the profile name of the AWS account
            aws_service (str): the Boto3 AWS service name to base an object on
        """
        self.profile_name = profile_name
        self.aws_service = aws_service
        self.aws_session_type = aws_session_type
        self.session = self.get_boto3_session(profile_name=profile_name)
        self.service_session = self.get_service_session(aws_service=aws_service)

    @staticmethod
    def get_boto3_session(profile_name: str = None) -> boto3.Session:
        """ Creation of the Boto3 session to connect to AWS services

        Args:
            profile_name (str):

        Returns:

        """
        return boto3.Session(profile_name=profile_name)

    def get_service_session(self, aws_service: str = None) -> boto3.Session.client:
        """

        Args:
            aws_service ():

        Returns:

        """
        if self.aws_session_type == "client":
            return self.session.client(aws_service)
        else:
            return self.session.resource(aws_service)

    def convert_to_json(self, object_to_convert):
        default = self.json_default_date_config(object_to_convert=object_to_convert)
        return json.dumps(object_to_convert, sort_keys=True, indent=2, default=default)

    @staticmethod
    def json_default_date_config(object_to_convert=None):
        if isinstance(object_to_convert, (datetime.date, datetime.datetime)):
            return object_to_convert.isoformat()


class Route53Helper(Boto3Helper):

    AWS_SERVICE = "route53"

    def __init__(self, dns_name=None, forward_name=None, **kwargs):
        kwargs.update({"aws_service": self.AWS_SERVICE})
        super().__init__(**kwargs)
        self.dns_name = dns_name
        self.forward_name = forward_name

    def get_domain_name(self, dns_name=None):
        """
        Scan our hosted zones for the record of a given name.
        Returns the record entry, else None.
        """

        if not dns_name:
            dns_name = self.dns_name

        try:
            zones = self.get_all_zones()
            for zone in zones["HostedZones"]:
                records = self.service_session.list_resource_record_sets(
                    HostedZoneId=zone["Id"]
                )
                for record in records["ResourceRecordSets"]:
                    if (
                        record["Type"] in ("CNAME", "A")
                        and record["Name"][:-1] == dns_name
                    ):
                        return record

        except Exception as e:
            logger.exception(e)
            return None

    def get_all_zones(self):
        """Same behaviour of list_host_zones, but transparently handling pagination."""
        zones = {"HostedZones": []}

        new_zones = self.service_session.list_hosted_zones(MaxItems="100")
        while new_zones["IsTruncated"]:
            zones["HostedZones"] += new_zones["HostedZones"]
            new_zones = self.service_session.list_hosted_zones(
                Marker=new_zones["NextMarker"], MaxItems="100"
            )

        zones["HostedZones"] += new_zones["HostedZones"]
        return zones

    def get_hosted_zone_id_for_domain(self, dns_name):
        """
        Get the Hosted Zone ID for a given domain.
        """
        all_zones = self.get_all_zones()
        return self.get_best_match_zone(all_zones, dns_name)

    @staticmethod
    def get_best_match_zone(all_zones, dns_name):
        """Return zone id which name is closer matched with domain name."""

        public_zones = [
            zone
            for zone in all_zones["HostedZones"]
            if not zone["Config"]["PrivateZone"]
        ]

        zones = {
            zone["Name"][:-1]: zone["Id"]
            for zone in public_zones
            if zone["Name"][:-1] in dns_name
        }
        if zones:
            keys = max(zones.keys(), key=lambda a: len(a))
            return zones[keys]
        else:
            return None

    def update_route53_records(
        self, dns_name=None, forward_name=None, action_type=None
    ):
        """
        Updates Route53 Records following domain creation
        """
        if not dns_name:
            dns_name = self.dns_name

        if not forward_name:
            forward_name = self.forward_name

        zone_id = self.get_hosted_zone_id_for_domain(dns_name)

        record_set = {
            "Name": dns_name,
            "Type": "CNAME",
            "ResourceRecords": [{"Value": forward_name}],
            "TTL": 60,
        }

        if action_type == "create":
            response = self.service_session.change_resource_record_sets(
                HostedZoneId=zone_id,
                ChangeBatch={
                    "Changes": [{"Action": "UPSERT", "ResourceRecordSet": record_set}]
                },
            )
        elif action_type == "delete":
            try:
                response = self.service_session.change_resource_record_sets(
                    HostedZoneId=zone_id,
                    ChangeBatch={
                        "Changes": [
                            {"Action": "DELETE", "ResourceRecordSet": record_set}
                        ]
                    },
                )
            except:
                print(
                    "Unable to delete record: {domain_name}".format(
                        domain_name=dns_name
                    )
                )
                response = None
        else:
            response = None

        return response


class ELBHelper(Boto3Helper):

    AWS_SERVICE = "elbv2"

    def __init__(
        self,
        load_balancer_name=None,
        target_group_name=None,
        dns_name=None,
        target_group_protocol=None,
        vpc_id=None,
        **kwargs,
    ):
        kwargs.update({"aws_service": self.AWS_SERVICE})
        super().__init__(**kwargs)
        self.load_balancer_name = load_balancer_name
        self.load_balancer_arn = self.get_load_balancer_arn_from_name()
        self.listener_arn = self.get_listener_arn_from_name()
        self.target_group_name = target_group_name
        self.dns_name = dns_name
        self.target_group_protocol = target_group_protocol
        self.vpc_id = vpc_id

    def get_load_balancer_arn_from_name(self, load_balancer_name=None):
        if not load_balancer_name:
            load_balancer_name = self.load_balancer_name

        load_balancers = self.service_session.describe_load_balancers().get(
            "LoadBalancers"
        )

        return [
            d for d in load_balancers if d["LoadBalancerName"] == load_balancer_name
        ][0].get("LoadBalancerArn")

    def get_listener_arn_from_name(self, load_balancer_arn=None):
        if not load_balancer_arn:
            load_balancer_arn = self.load_balancer_arn

        listeners = self.service_session.describe_listeners(
            LoadBalancerArn=load_balancer_arn
        ).get("Listeners")

        return [d for d in listeners if d["Protocol"] == "HTTPS"][0].get("ListenerArn")

    def get_rules(self, as_json=False):
        response = self.service_session.describe_rules(ListenerArn=self.listener_arn)
        if as_json:
            return self.convert_to_json(response)
        else:
            return response

    def get_priority_list(self):

        response = self.get_rules().get("Rules")

        priorities = [
            int(x.get("Priority")) for x in response if x.get("Priority").isdigit()
        ]

        return priorities

    def get_rule_arn(self):
        response = self.get_rules().get("Rules")

        for item in response:
            conditions = item.get("Conditions")
            for condition in conditions:
                if condition.get("Field") == "host-header":
                    if condition.get("Values")[0] == self.dns_name:
                        return item.get("RuleArn")

        return None

    def create_rule(self):
        priority = max(self.get_priority_list()) + 1
        target_group_arn = self.get_target_group_arn()

        response = self.service_session.create_rule(
            ListenerArn=self.listener_arn,
            Conditions=[{"Field": "host-header", "Values": [self.dns_name]}],
            Priority=priority,
            Actions=[
                {"Type": "forward", "TargetGroupArn": target_group_arn, "Order": 1}
            ],
        )
        return response

    def delete_rule(self):
        rule_arn = self.get_rule_arn()

        if rule_arn:
            response = self.service_session.delete_rule(RuleArn=rule_arn)
            return response
        else:
            return None

    def get_target_group(self, as_json=False):
        try:
            response = self.service_session.describe_target_groups(
                Names=[self.target_group_name]
            )
            if as_json:
                output = self.convert_to_json(response)
            else:
                output = response

        except:
            output = None

        return output

    def get_target_group_arn(self):
        try:
            return self.get_target_group().get("TargetGroups")[0].get("TargetGroupArn")
        except:
            return None

    def create_target_group(self, as_json=False):

        try:
            response = (
                self.service_session.create_target_group(
                    Name=self.target_group_name,
                    Protocol=self.target_group_protocol,
                    Port=80,
                    VpcId=self.vpc_id,
                    HealthCheckProtocol="HTTP",
                    HealthCheckEnabled=True,
                    HealthCheckPath="/",
                    HealthCheckIntervalSeconds=30,
                    HealthCheckTimeoutSeconds=5,
                    HealthyThresholdCount=5,
                    UnhealthyThresholdCount=2,
                    Matcher={"HttpCode": "200,404"},
                    TargetType="instance",
                ),
            )
            if as_json:
                output = self.convert_to_json(response)
            else:
                output = response
            return output
        except Exception as e:
            raise e

    def delete_target_group(self):

        try:
            target_group_arn = self.get_target_group_arn()
            response = self.service_session.delete_target_group(
                TargetGroupArn=target_group_arn
            )
            return response
        except:
            return None


class ECSHelper(Boto3Helper):

    AWS_SERVICE = "ecs"

    def __init__(
        self,
        task_definition_name=None,
        family_name=None,
        cluster_name=None,
        load_balancer_name=None,
        target_group_arn=None,
        application_name=None,
        application_port=None,
        proxy_image_name=None,
        proxy_port=None,
        task_role_arn=None,
        execution_role_arn=None,
        **kwargs,
    ):
        kwargs.update({"aws_service": self.AWS_SERVICE})
        super().__init__(**kwargs)
        self.task_definition_name = task_definition_name
        self.family_name = family_name
        self.cluster_name = cluster_name
        self.load_balancer_name = load_balancer_name
        self.target_group_arn = target_group_arn
        self.application_name = application_name
        self.application_port = application_port
        self.proxy_image_name = proxy_image_name
        self.proxy_port = proxy_port
        self.task_role_arn = task_role_arn
        self.execution_role_arn = execution_role_arn

    def get_task_definition(self):
        try:
            response = self.service_session.describe_task_definition(
                taskDefinition=self.family_name
            )
        except:
            response = None

        return response

    def create_task_definition(self):
        task_definition_exists = self.get_task_definition()

        if not task_definition_exists:
            response = self.service_session.register_task_definition(
                family=self.family_name,
                networkMode="bridge",
                taskRoleArn=self.task_role_arn,
                executionRoleArn=self.execution_role_arn,
                containerDefinitions=generate_template(
                    application_name=self.application_name,
                    application_port=self.application_port,
                ),
            )

            return response

        else:
            return None

    def get_ecs_service(self):
        response = self.service_session.describe_services(
            cluster=self.cluster_name, services=[self.family_name]
        )
        failure = response.get("failures")
        if failure:
            return None
        else:
            if not response.get("services")[0].get("status") == "INACTIVE":
                return response.get("services")[0].get("serviceArn")
            else:
                return None

    def create_ecs_service(self):
        response = self.service_session.create_service(
            cluster=self.cluster_name,
            serviceName=self.family_name,
            taskDefinition=self.family_name,
            loadBalancers=[
                {
                    "targetGroupArn": self.target_group_arn,
                    "containerName": self.proxy_image_name,
                    "containerPort": self.proxy_port,
                }
            ],
            desiredCount=1,
            launchType="EC2",
            healthCheckGracePeriodSeconds=120,
            schedulingStrategy="REPLICA",
            deploymentController={"type": "ECS"},
            tags=[{"key": "User:Application", "value": self.family_name}],
            enableECSManagedTags=False,
        )

        return response

    def delete_ecs_service(self):
        try:
            response_1 = self.service_session.update_service(
                cluster=self.cluster_name,
                service=self.family_name,
                desiredCount=0,
                taskDefinition=self.task_definition_name,
                forceNewDeployment=True,
            )
        except:
            response_1 = None

        try:
            response_2 = self.service_session.delete_service(
                cluster=self.cluster_name, service=self.family_name, force=True
            )
            # TODO check if service is inactive
        except:
            response_2 = None

        return response_1, response_2

    def update_target_group_arn(self, target_group_arn=None):
        self.target_group_arn = target_group_arn

    def delete_task_registration(self):
        while True:
            try:
                response = self.service_session.deregister_task_definition(
                    taskDefinition=self.get_task_definition()
                    .get("taskDefinition")
                    .get("taskDefinitionArn")
                )
            except:
                logger.info("Task registration deleted...")
                response = None
                break
            else:
                break

        return response


class S3Helper(Boto3Helper):

    AWS_SERVICE = "s3"

    def __init__(self, bucket_name=None, prefix=None, **kwargs):
        kwargs.update({"aws_session_type": "resource"})
        kwargs.update({"aws_service": self.AWS_SERVICE})
        super().__init__(**kwargs)
        self.bucket_name = bucket_name
        self.prefix = prefix

    def get_file(self, file_name=None):

        if not file_name:
            return None

        prefix = self.prefix

        if prefix[-1] != "/":
            prefix = prefix + "/"

        s3_object = self.service_session.Object(self.bucket_name, prefix + file_name)
        result = s3_object.get()["Body"].read().decode("utf-8")

        return result

    def upload_file(self, file_name=None, body=None, json_file=True):
        if not file_name:
            return None

        prefix = self.prefix

        if prefix[-1] != "/":
            prefix = prefix + "/"

        s3_object = self.service_session.Object(
            self.bucket_name, f"{prefix}{file_name}"
        )

        if json_file:
            response = s3_object.put(
                Body=(bytes(json.dumps(body).encode("UTF-8"))),
                Metadata={"place_id": file_name},
            )
        else:
            response = s3_object.put(
                Body=body,
                Metadata={"place_id": file_name},
            )

        try:
            result = response.get("ResponseMetadata").get("HTTPStatusCode")
            if result == 200:
                return True
            else:
                return False
        except Exception as e:
            logger.exception(e)
            return False

# BotoHelper()
# s3 = self.get_s3_object(profile_name=profile_name)
#
# s3_object = s3.Object(bucket_name, "configuration/" + config_file)
#
# s3_credentials = s3_object.get()["Body"].read().decode("utf-8")
#
# credentials = io.StringIO(s3_credentials)
